# Práctica 2 - VueJs 3

Práctica 2 del Módulo 6 Vuejs del Diplomado Fullstack Backend y Frontend v3.

## Restaurar Proyecto

```
npm install
```

### Variables de Entorno

Copiar el archivo .env.example como .env en la misma ubicación

```
cp .env.example .env
```

### Habilitar API Rest

Abrir una consola en la ubicacion del siguiente directorio: /db, donde se encuentra el archivo db.json y ejecutar el siguiente comando:

`json-server --watch db.json`

### Iniciar el Proyecto

Iniciar el proyecto con el siguiente comando

```
npm run serve
```

### Autores

Franz Javier Muraña Cruz
